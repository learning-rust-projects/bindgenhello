const wasm = import('./wasm-dist/bindgenhello');

wasm
  .then(h => h.hello("world"))
  .catch(console.error);